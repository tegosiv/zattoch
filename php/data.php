<?php
include_once('loterea.php');

$loterea = new Loterea;

$act = $_GET['act'];
$result = array();

switch ($act) {
    case 'today':
        $result = array('today' => $loterea->today());

        break;

    case 'ticket':
        $result = $loterea->getNewTicket();

        break;
    default:
        break;
}

echo json_encode($result);