<?php
define('DIR_CACHE', 'file/');

final class  Loterea
{
    private $expire;
    private $ip;
    private $prizes = array();
    private $prize;

    public function __construct($expire = 86400)
    {
        $this->expire = $expire;
        $this->ip = $this->get_client_ip();

        $files = glob(DIR_CACHE . 'file.*');

        if ($files) {
            foreach ($files as $file) {
                $time = substr(strrchr($file, '.'), 1);

                if ($time < time()) {
                    if (file_exists($file)) {
                        unlink($file);
                    }
                }
            }
        }
        $this->setPrizes();
    }

    public function setPrizes()
    {
        $this->prizes[] = array('id' => 0, 'name' => 'ZATTOCH nano 5', 'pers' => 0.1);
        $this->prizes[] = array('id' => 1, 'name' => 'Скидку 50% на приобретение ZATTOCH', 'pers' => 0.2);
        $this->prizes[] = array('id' => 2, 'name' => 'Скидку 25% на приобретение ZATTOCH', 'pers' => 0.21);
        $this->prizes[] = array('id' => 3, 'name' => 'Бесплатную доставку', 'pers' => 0.22);
        $this->prizes[] = array('id' => 4, 'name' => 'К сожалению, Вы не выиграли', 'pers' => 100);
    }

    public function getTicket($ip)
    {
        $files = glob(DIR_CACHE . 'file.' . $ip . '.*');
        if ($files) {
            $handle = fopen($files[0], 'r');
            flock($handle, LOCK_SH);
            $data = fread($handle, filesize($files[0]));
            flock($handle, LOCK_UN);
            fclose($handle);
            $d = unserialize($data);
            $d['not'] = true;
            return $d;
        }
        return false;
    }

    public function getNewTicket()
    {

        $files = glob(DIR_CACHE . 'file.' . $this->ip . '.*');
        if (!$files) {
            $rand = rand(0, 100);
            $rand = $rand / 100.0;


            foreach ($this->prizes as $prize) {
                $pers = $prize['pers'] / 100;
                if ($rand < $pers) {
                    $this->prize = $prize;
                    break;
                }
            }

            $this->write();


        }else{
            $this->prize = $this->getTicket($this->ip);
        }
        return $this->prize;
    }

    public function today()
    {
        $files = glob(DIR_CACHE . 'file.' . $this->ip . '.*');
        if (!$files)
            return true;
        return false;
    }

    public function get_client_ip()
    {
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }


    public function write()
    {
        $this->delete($this->ip);
        $file = DIR_CACHE . 'file.' . $this->ip . '.' . (time() + $this->expire);
        $handle = fopen($file, 'w');
        flock($handle, LOCK_EX);
        fwrite($handle, serialize($this->prize));
        fflush($handle);
        flock($handle, LOCK_UN);
        fclose($handle);
    }

    public function delete($ip)
    {
        $files = glob(DIR_CACHE . 'file.' . preg_replace('/[^A-Z0-9\._-]/i', '', $ip) . '.*');

        if ($files) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }


}