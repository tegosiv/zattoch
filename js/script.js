var timeLoadPage = 0;
var preloaderTimer = {};
var minimumTimeShowPreloader = 6000;
var isLoadedPage = false;
var v_easing = 'easeIn';

$(window).load(function () {
    timeLoadPage = Date.now() - timerStart;
    isLoadedPage = true;
    //var loadTime = window.performance.timing.domContentLoadedEventEnd- window.performance.timing.navigationStart;
});

$(document).ready(function () {

    Animation.setFrameRate(55);
    $('.go-to-second').click(function () {
        $('a[data-index="2"]').trigger('click');
    });

    Animation.makePathFromSvg('two_circle_line', '#two_circle_line');

    Animation.makePathFromSvg('two__line', '#two__line');
    Animation.makePathFromSvg('svg_border_anim', '#svg_border_anim');


    //Definition Animation
    $('#first_black').tween({
        opacity: {
            start: 0,
            stop: 100,
            time: 6.27,
            duration: 0.86,
            effect: v_easing,
            onStop: function () {
                $('.preloader').remove();
                $('#first_black').fadeOut(870);
            }
        }
    }).play();

    $('.section[data-index="1"]').tween({
        transform: {
            start: 'scale( 1.1 )',
            stop: 'scale(1.00)',
            time: 7.13,
            duration: 1.1
        }
    }).play();

    $('.zattoch-info').tween({
        opacity: {
            start: 0,
            stop: 100,
            time: 8.26,
            duration: 0.87,
            effect: v_easing
        }
    }).play();

    $('.light-left').tween({
        left: {
            start: 15,
            stop: 22,
            time: 8.43,
            units: '%',
            duration: 1.61,
            effect: v_easing
        },
        bottom: {
            start: -5,
            stop: 2,
            time: 8.43,
            units: '%',
            duration: 1.61,
            effect: v_easing
        },
        opacity: {
            start: 0,
            stop: 100,
            time: 8.43,
            duration: 1.61,
            effect: v_easing
        },
        onStop: function () {
            $('.light-left').addClass('rotating-light-left');
        }
    }).play();

    $('.light-right').tween({
        opacity: {
            start: 0,
            stop: 100,
            time: 8.43,
            duration: 1.61,
            effect: v_easing
        },
        onStop: function () {
            $('.light-right').addClass('rotating-light-right');
            $('#diamond_1').addClass('diamond_1_animation');
            $('#diamond_2').addClass('diamond_2_animation');
        }
    }).play();

    $('.dark-bottom').tween({
        opacity: {
            start: 0,
            stop: 100,
            time: 8.43,
            duration: 1.05,
            effect: v_easing
        }
    }).play();

    $('.go-to-second').tween({
        opacity: {
            start: 0,
            stop: 100,
            time: 11.09,
            duration: 0.5,
            effect: v_easing
        }
    }).play();

    $('#two_circle_line').tween({
        opacity: {
            start: 0,
            stop: 100,
            time: 9.0,
            duration: 0.11,
            effect: v_easing,
            onStop: function () {
                var two_circle_line = new SvgAnimation({
                    "elem": "#two_circle_line",
                    "increaseBy": 2,
                    "timeout": 7,
                    "stroke": 'white'
                });
                two_circle_line.animate();
            }
        }
    }).play();

    $('#svg_border_anim').tween({
        opacity: {
            start: 0,
            stop: 100,
            time: 10.0,
            duration: 0.32,
            effect: v_easing,
            onStop: function () {
                var svg_border_anim = new SvgAnimation({
                    "elem": "#svg_border_anim",
                    "increaseBy": 2,
                    "timeout": 7,
                    "stroke": 'white'
                });
                svg_border_anim.animate();

            }
        }
    }).play();

    $('.onepage-pagination, .page-nav').tween({
        opacity: {
            start: 0,
            stop: 100,
            time: 9.47,
            duration: 1.0,
            effect: v_easing
        }
    }).play();

    $('#text_more').tween({
        opacity: {
            start: 0,
            stop: 100,
            time: 10.47,
            duration: 1.0,
            effect: v_easing
        },
        onStop: function () {
            var replace = $('<a href="#"><span>ПОДРОБНЕЕ</span></a>');
            $('.div_block_with_border').replaceWith(replace);

        }
    }).play();

    $('.lang').tween({
        opacity: {
            start: 0,
            stop: 100,
            time: 10.33,
            duration: 0.75,
            effect: v_easing
        }
    }).play();


    preloaderTimer = setInterval(function () {
        checkPreloader();
    }, 1000);

    var block_1_element = $('.page-nav, .lang, ' +
        '.zattoch-info, .go-to-second, ' +
        '.onepage-pagination, .light-left, .light-right, .dark-bottom, #text_more');
    block_1_element
        .css('opacity', '0');

    //var container = document.getElementById('canvas');
    //init(container, 531, 438, '#fff');

    Monet.init();


});


// monet
var Monet = {
    elem: $('#erase_block'),
    ctx: 0,
    timer: 0,
    server: 'http://zattoch.noisy2.ru/',
    mayDraw: false,
    mayDrawServer: false,
    message: '',
    prize: {},
    isBlack: function (r, g, b) {
        var x = 18;
        var y = 18;
        var z = 18;
        var diff = 7;
        if (Math.abs(x - r) < diff
            && Math.abs(y - g) < diff
            && Math.abs(z - b) < diff
        ) {
            return true;
        }
        return false;

    },

    finish: function () {
        $.cookie('today', '1', {expires: 1});
        $('#buy_after').text(Monet.message);
        $('#buy_after').fadeIn(800);
        $('#erase_block').fadeOut(1300);
        if (Monet.prize.id == 4) {
            $('#buy_after').prop('disabled', true);
        }
    },

    isEnd: function () {
        var colorThief = new ColorThief();
        var can = document.getElementById("erase_block");

        var cc = colorThief.getColor(can, 10);


        if (!this.isBlack(cc[0], cc[1], cc[2])) {

            $('#canvas_erase_block').removeClass('cursorcoin');
            Monet.finish();
        }
        //$('body').css('background', 'rgb('+cc+')');
        //var dominantColor = getDominantColor(dataURL);
    },

    check: function () {
        if ($.cookie('today') != 1 && this.mayDrawServer) {
            return true;
        }
        return false;
    },

    init: function () {
        var t = this;

        var c = document.getElementById("erase_block");
        this.ctx = c.getContext("2d");

        var base_image = new Image();
        base_image.src = 'img/monet/erase_first.png';
        base_image.onload = function () {
            Monet.ctx.drawImage(base_image, 0, 0);
        };

        $.ajax(
            {
                url: this.server + 'php/data.php?act=today',
                type: "POST",
                dataType: "json",
                async: false,
                success: function (result) {
                    Monet.mayDrawServer = result.today;
                    //alert(result);
                },
                error: function () {
                    Monet.mayDrawServer = false;
                }
            });

        if ($.cookie('today') != 1 || 1 /*&& this.mayDrawServer*/) {
            $.ajax(
                {
                    url: this.server + 'php/data.php?act=ticket',
                    type: "POST",
                    dataType: "json",
                    async: false,
                    success: function (result) {
                        //Monet.mayDrawServer = false;
                        Monet.prize = result;
                        Monet.message = result.name;
                        var src = 'img/monet/' + result.id + '.jpg';
                        $('#gift img').attr('src', src);
                        if (result.not) {
                            Monet.finish();
                        }
                    },
                    error: function () {
                        Monet.mayDrawServer = false;
                    }
                });
        }


        this.ctx.fillCircle = function (x, y, radius, fillColor) {
            this.fillStyle = fillColor;
            this.beginPath();
            this.moveTo(x, y);
            this.arc(x, y, radius, 0, Math.PI * 2, false);
            this.fill();
        };


        this.ctx.clearTo = function (fillColor) {
            Monet.ctx.fillStyle = fillColor;
            Monet.ctx.fillRect(0, 0, 360, 95);
        };
        //this.ctx.clearTo("#ff00ff");


        t.elem.mouseup(function () {
            t.mayDraw = false;
            Monet.isEnd();
            return false;
        }).mousedown(function (e) {

            t.mayDraw = true;
            return false;
        });


        t.elem.mousemove(function (e) {
            if (t.mayDraw) {
                if (Monet.check()) {
                    var offset = $(this).offset();
                    var x = (e.pageX - offset.left);
                    var y = (e.pageY - offset.top);

                    var radius = 20;
                    var fillColor = '#ff0';
                    Monet.ctx.globalCompositeOperation = 'destination-out';
                    Monet.ctx.fillCircle(x, y, radius, fillColor);
                }
            }
        });

        if (Monet.check()) {
            $('#canvas_erase_block').addClass('cursorcoin');
        } else {
            $('#canvas_erase_block').addClass('not-cursor');
        }

        /*
         */


    },

    hide: function () {
        var op = this.elem.css('opacity');
        alert(op);
        op -= 0.04;
        this.elem.css('opacity', op + '');
    },

    addCircle: function (x, y) {
        var circle = $('<div/>');
        circle.addClass('circle');
        circle.css({top: y + 10 + 'px', left: x + 10 + 'px'});
        $('#insert_circle').append(circle);
    },
    addClipCircle: function (x, y) {
        var circle = $('<div />');
        circle.css('-webkit-clip-path', 'circle(30px at ' + x + 'px ' + y + 'px)');
        //circle.css({top: y + 10 + 'px', left: x + 10 + 'px'});
        $('#insert_circle').append(circle);
    }


}


function checkPreloader() {
    if (minimumTimeShowPreloader > 0) {
        if (isLoadedPage) {
            minimumTimeShowPreloader -= 1001;
        }
    } else {
        removePreloader();
    }
}

function removePreloader() {
    clearInterval(preloaderTimer);
}

// in feature
function runAfter(callback, time) {
    setTimeout(callback, time);
}

var Animation = {
    setFrameRate: function (rate) {
        $.framerate(rate);
    },
    stateSection: [],
    checkState: function (index) {
        if (this.stateSection[index]) {
            return true;
        }
        return false;
    },
    setState: function (index_section, status) {
        this.stateSection[index_section] = status;
    },
    inst: this,
    section2animation: function () {
        this.setState(2, true);
        $('section[data-index="2"] .left .title').tween({
            opacity: {
                start: 0,
                stop: 100,
                time: 0.2,
                duration: 0.3,
                effect: v_easing
            }
        }).play();

        $('section[data-index="2"] .left .m-italic').tween({
            opacity: {
                start: 0,
                stop: 100,
                time: .37,
                duration: .3,
                effect: v_easing
            }
        }).play();

        $('#p_div_section_2').tween({
            opacity: {
                start: 0,
                stop: 100,
                time: .5,
                duration: 0.7,
                effect: v_easing
            }
        }).play();


        $('#zatochka_1_section_2').tween({
            transform: {
                start: 'scale( .7 )',
                stop: 'scale(1.00)',
                time: 0.67,
                duration: 0.85
            },
            opacity: {
                start: 0,
                stop: 100,
                time: .67,
                duration: 0.85,
                effect: v_easing
            }
        }).play();

        $('#zatochka_2_section_2').tween({
            left: {
                start: 100,
                stop: -85,
                time: 1.77,
                units: 'px',
                duration: .6,
                effect: v_easing,
                onStop: function () {
                    var two___line = new SvgAnimation({
                        "elem": "#two__line",
                        increaseBy: 3,
                        "timeout": 3,
                        "stroke": 'black'
                    });
                    two___line.animate();
                }
            },
            opacity: {
                start: 0,
                stop: 100,
                time: 1.77,
                duration: .2,
                effect: v_easing
            }
        }).play();


        $('#two__line').tween({
            opacity: {
                start: 0,
                stop: 100,
                time: 2.05,
                duration: 0.35,
                effect: v_easing
            }
        }).play();


    },

    preparesection2animation: function () {
        var el = $('.left ').find('.title, .m-italic, #p_div_section_2');
        Animation.hide(el);
        $('#zatochka_1_section_2').css('transform', 'scale(.7)');
        $('#zatochka_1_section_2, #zatochka_2_section_2').css('opacity', '0');
        $('#zatochka_2_section_2').css('left', '100px');
        this.prepareSvgToAnimation('#two__line');
    },

    preparesection3animation: function () {
        var where = 'section[data-index="3"]';
        var el = $(where).find('.layers-wrap img, .right .title,' +
            '.right .what-is .row span:nth-of-type(1), .right .what-is .row .icon, .right .what-is .row .text,' +
            '.must-know, .right .btm-text');
        Animation.hide(el);
        $(where).find('.layers-wrap img').css('top', '-10px');
        $(where).find('.layers-wrap img:eq(0)').css('top', '-10%');
        //$(where).find('.right .what-is .row .line').css('min-width', '200px');
        $('#section3hidefisrtblock').css('left', '0');

    },

    section3animation: function () {
        this.setState(3, true);
        var where = 'section[data-index="3"]';
        var delay = 0.3;


        $(where + ' #section3hidefisrtblock').tween({
            left: {
                start: 0,
                stop: 100,
                time: delay + 0,
                duration: 0.5,
                units: '%',
                effect: v_easing,
                onStop: function () {
                    //$(where + ' #section3hidefisrtblock').remove();
                }
            }
        }).play();


        $(where).find('.layers-wrap img').tween({
            opacity: {
                start: 0,
                stop: 100,
                time: delay + .2,
                duration: .37,
                effect: v_easing
            },
            top: {
                start: -$(window).height() * 0.1,
                stop: -10,
                time: delay + .2,
                duration: .37,
                units: 'px',
                effect: v_easing
            }
        }).play();

        $(where).find('.layers-wrap img:eq(1)').tween({
            opacity: {
                start: 0,
                stop: 100,
                time: delay + .2,
                duration: .37,
                effect: v_easing
            },
            top: {
                start: -10,
                stop: 60,
                time: delay + .63,
                duration: 1.17,
                units: 'px',
                effect: v_easing
            }
        }).play();

        $(where).find('.layers-wrap img:eq(2)').tween({
            opacity: {
                start: 0,
                stop: 100,
                time: delay + .2,
                duration: .37,
                effect: v_easing
            },
            top: {
                start: -10,
                stop: 130,
                time: delay + .63,
                duration: 1.17,
                units: 'px',
                effect: v_easing
            }
        }).play();

        $(where).find('.layers-wrap img:eq(3)').tween({
            opacity: {
                start: 0,
                stop: 100,
                time: delay + .2,
                duration: .37,
                effect: v_easing
            },
            top: {
                start: -10,
                stop: 200,
                time: delay + .63,
                duration: 1.17,
                units: 'px',
                effect: v_easing
            }
        }).play();


        $(where).find('.right .title').tween({
            opacity: {
                start: 0,
                stop: 100,
                time: delay + .27,
                duration: 0.3,
                effect: v_easing
            }
        }).play();


        $(where).find('.right .what-is .row .line').tween({
            width: {
                start: 0,
                stop: 200,
                time: delay + .33,
                units: 'px',
                duration: 0.34
            }
        }).play();


        $(where).find('.right .what-is .row span:nth-of-type(1)').tween({
            opacity: {
                start: 0,
                stop: 100,
                time: delay + .53,
                duration: .33,
                effect: v_easing
            }
        }).play();

        $(where).find('.right .what-is .row .icon, .right .what-is .row .text ').tween({
            opacity: {
                start: 0,
                stop: 100,
                time: delay + .74,
                duration: .55,
                effect: v_easing
            }
        }).play();

        $(where).find('.must-know, .right .btm-text').tween({
            opacity: {
                start: 0,
                stop: 100,
                time: delay + 1.43,
                duration: .5,
                effect: v_easing
            }
        }).play();


        /*
         $('#two__line').tween({
         opacity: {
         start: 0,
         stop: 100,
         time: 2.05,
         duration: 0.35,
         effect: v_easing
         }
         }).play();
         */


    },

    hide: function (obj) {
        $(obj).css('opacity', '0');
    },

    prepareSvgToAnimation: function (obj) {
        $(obj).css({'opacity': '0'});

        $(obj).find('path').css('stroke-dasharray', '');
        $(obj).find('path').css('stroke-dashoffset', '');
        $(obj).find('path').attr('stroke', 'transparent');
    },

    makePathFromSvg: function (selector, reset) {
        var t = this;
        var viv = new Vivus(selector, {
            type: 'scenario-sync',
            duration: 10,
            pathTimingFunction: Vivus.EASE_OUT
        }, function () {
            t.prepareSvgToAnimation(reset);
        });
        viv.finish();
    }


}


function createCanvas(parent, width, height) {
    var canvas = {};
    canvas.node = document.createElement('canvas');
    canvas.context = canvas.node.getContext('2d');
    canvas.node.width = width || 100;
    canvas.node.height = height || 100;
    parent.appendChild(canvas.node);
    return canvas;
}

function init(container, width, height, fillColor) {
    var canvas = createCanvas(container, width, height);
    var ctx = canvas.context;
    // define a custom fillCircle method
    ctx.fillCircle = function (x, y, radius, fillColor) {
        this.fillStyle = fillColor;
        this.beginPath();
        this.moveTo(x, y);
        this.arc(x, y, radius, 0, Math.PI * 2, false);
        this.fill();
    };
    ctx.clearTo = function (fillColor) {
        ctx.fillStyle = fillColor;
        ctx.fillRect(0, 0, width, height);
    };
    ctx.clearTo(fillColor || "#ff00ff");

    // bind mouse events
    canvas.node.onmousemove = function (e) {
        if (!canvas.isDrawing) {
            return;
        }
        var x = e.pageX - this.offsetLeft;
        var y = e.pageY - this.offsetTop;
        var radius = 10; // or whatever
        var fillColor = '#ff0';
        ctx.globalCompositeOperation = 'destination-out';
        ctx.fillCircle(x, y, radius, fillColor);
    };
    canvas.node.onmousedown = function (e) {
        canvas.isDrawing = true;
    };
    canvas.node.onmouseup = function (e) {
        canvas.isDrawing = false;
    };
}



