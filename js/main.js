jQuery(document).ready(function ($) {

    $(".blocks").onepage_scroll({
        sectionContainer: "section",    // sectionContainer accepts any kind of selector in case you don't want to use section
        easing: "ease",                 // Easing options accepts the CSS3 easing animation such "ease", "linear", "ease-in",
        // "ease-out", "ease-in-out", or even cubic bezier value such as "cubic-bezier(0.175, 0.885, 0.420, 1.310)"
        animationTime: 600,             // AnimationTime let you define how long each section takes to animate
        pagination: true,               // You can either show or hide the pagination. Toggle true for show, false for hide.
        updateURL: false,               // Toggle this true if you want the URL to be updated automatically when the user scroll to each page.
        beforeMove: function (index) {

            switch (index) {
                case 2:
                    if (!Animation.checkState(index)) {
                        Animation.preparesection2animation();
                    }
                    break;
                case 3:
                    if (!Animation.checkState(index) || 1) {
                        Animation.preparesection3animation();
                    }
                    break;
            }
        },
        afterMove: function (index) {
            switch (index) {
                case 2:
                    if (!Animation.checkState(index))
                        Animation.section2animation();
                    break;
                case 3:
                    if (!Animation.checkState(index) || 1)
                        Animation.section3animation();
                    break;
            }

        },
        loop: false,
        keyboard: false,
        responsiveFallback: 1200,
        direction: "vertical"
    });

    $('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function () {
        $(this).toggleClass('open');
        $(this).parent().toggleClass('open');
        $('.full-page-menu').toggleClass('open');
    });
})
;


onResize = function() {
    var seventhScrean = $('.blocks section:nth-of-type(7)').height();
    $('.rounded-bg').height(seventhScrean);

    if (window.matchMedia('(min-width: 1200px)').matches){
        $('.blocks section').removeAttr('id');

        $(".menu li:nth-of-type(1) a").on( "click", function() {
            $( ".onepage-pagination a[data-index='1']" ).click();
            $('.open-close-nav, .full-page-menu').removeClass('open');
            $(this).parent('li').addClass('current').siblings('li').removeClass('current');
        });
        $(".menu li:nth-of-type(2) a").on( "click", function() {
            $( ".onepage-pagination a[data-index='2']" ).click();
            $('.open-close-nav, .full-page-menu').removeClass('open');
            $(this).parent('li').addClass('current').siblings('li').removeClass('current');
        });
        $(".menu li:nth-of-type(3) a").on( "click", function() {
            $( ".onepage-pagination a[data-index='3']" ).click();
            $('.open-close-nav, .full-page-menu').removeClass('open');
            $(this).parent('li').addClass('current').siblings('li').removeClass('current');
        });
        $(".menu li:nth-of-type(4) a").on( "click", function() {
            $( ".onepage-pagination a[data-index='4']" ).click();
            $('.open-close-nav, .full-page-menu').removeClass('open');
            $(this).parent('li').addClass('current').siblings('li').removeClass('current');
        });
        $(".menu li:nth-of-type(5) a").on( "click", function() {
            $( ".onepage-pagination a[data-index='5']" ).click();
            $('.open-close-nav, .full-page-menu').removeClass('open');
            $(this).parent('li').addClass('current').siblings('li').removeClass('current');
        });
        $(".menu li:nth-of-type(6) a").on( "click", function() {
            $( ".onepage-pagination a[data-index='6']" ).click();
            $('.open-close-nav, .full-page-menu').removeClass('open');
            $(this).parent('li').addClass('current').siblings('li').removeClass('current');
        });
        $(".menu li:nth-of-type(7) a").on( "click", function() {
            $( ".onepage-pagination a[data-index='7']" ).click();
            $('.open-close-nav, .full-page-menu').removeClass('open');
            $(this).parent('li').addClass('current').siblings('li').removeClass('current');
        });
        $(".menu li:nth-of-type(8) a").on( "click", function() {
            $( ".onepage-pagination a[data-index='8']" ).click();
            $('.open-close-nav, .full-page-menu').removeClass('open');
            $(this).parent('li').addClass('current').siblings('li').removeClass('current');
        });
        $(".menu li:nth-of-type(9) a").on( "click", function() {
            $( ".onepage-pagination a[data-index='9']" ).click();
            $('.open-close-nav, .full-page-menu').removeClass('open');
            $(this).parent('li').addClass('current').siblings('li').removeClass('current');
        });
        $(".menu li:nth-of-type(10) a").on( "click", function() {
            $( ".onepage-pagination a[data-index='10']" ).click();
            $('.open-close-nav, .full-page-menu').removeClass('open');
            $(this).parent('li').addClass('current').siblings('li').removeClass('current');
        });
    }
    else if (window.matchMedia('(max-width: 1199px)').matches) {

        $('.menu a').click(function() {
            $('.open-close-nav, .full-page-menu, .page-nav').removeClass('open');
        })
        // Scroll to second block
        $('.menu a[href^="#"]').on('click',function (e) {
            e.preventDefault();

            var target = this.hash;
            var $target = $(target);

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 800, 'swing', function () {
                window.location.hash = target;
            });
        });

        $(".menu a").on( "click", function() {
            $('.open-close-nav, .full-page-menu').removeClass('open');
        });
    }
}
$(document).ready(onResize);
$(window).bind('resize', onResize);


var firstSection = $('.blocks section:nth-of-type(1)').height();
$(window).scroll(function () {
    if ($(window).scrollTop() > firstSection - 60) {
        $('.page-nav').css({'background': '#000'});
    } else {
        $('.page-nav').css({'background': 'transparent'});
    }
});